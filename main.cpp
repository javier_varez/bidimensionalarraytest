
#include <string>
#include <iostream>
#include <chrono>

using namespace std;

struct Timer {
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    std::chrono::duration<float> duration;
    std::string name;

    Timer(const std::string &str) :
        name(str)
    {
        start = std::chrono::high_resolution_clock::now();
    }

    ~Timer() {
        end = std::chrono::high_resolution_clock::now();
        duration = end - start;
        std::cout << "Timer " << name << " took " << duration.count() << std::endl;
    }
};

void unidimensionalArray(int xLen, int yLen) {
    Timer timer("UnidimensionalTimer");
    int *array = new int[xLen*yLen];

    {
        Timer timer2("UnidimensionalInternal");
        int k = 0;
        for (int i = 0; i < xLen; i++) {
            for (int j = 0; j < yLen; j++) {
                array[i*yLen+j] = k++;
            }
        }
    }

    delete[] array;
}

void bidimensionalArray(int xLen, int yLen) {
    Timer timer("BidimensionalTimer");
    int **array = new int*[xLen];

    for (int i = 0; i < xLen; i++) {
        array[i] = new int[yLen];
    }

    {
        Timer timer2("BidimensionalInternal");
        int k = 0;
        for (int i = 0; i < xLen; i++) {
            for (int j = 0; j < yLen; j++) {
                array[i][j] = k++;
            }
        }
    }

    for (int i = 0; i < xLen; i++) {
        delete[] array[i];
    }
    delete[] array;
}

int main(int argc, char *argv[]) {
    if (argc < 3) {
        std::cout << "Please, indicate x and y sizes" << std::endl;
        return 0;
    }

    unsigned long long int x, y;
    x = strtoull(argv[1], nullptr, 10);
    y = strtoull(argv[2], nullptr, 10);

    unidimensionalArray(x, y);
    bidimensionalArray(x, y);

    return 0;
}
